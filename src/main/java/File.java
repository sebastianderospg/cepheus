import java.util.Date;

/**
 * Created by Stefan on 10.11.2016.
 */
public class File
{
    //tag::variables[]
    public String name;
    public String type;
    public String filepath;
    public String user;
    public Date   date;
    public double filesize;
    //end::variables[]

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        if (type != null || type.length() > 0 || !type.equals(""))
        {
            this.type = type;
        }
    }

    public String getFilepath()
    {
        return filepath;
    }

    public void setFilepath(String filepath)
    {
        this.filepath = filepath;
    }

    public String getUser()
    {
        return user;
    }

    public void setUser(String user)
    {
        if (type != null || type.length() > 0 || !type.equals(""))
        {
            this.user = user;
        }
    }

    public double getFilesize()
    {
        return filesize;
    }

    public void setFilesize(double filesize)
    {
        this.filesize = filesize;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        if (type != null || type.length() > 0 || !type.equals(""))
        {
            this.name = name;
        }
    }

    public Date getDate()
    {
        return date;
    }

    public void setDate(Date date)
    {
        if (date != null)
        {
            this.date = date;
        }
    }
}
