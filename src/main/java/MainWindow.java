/**
 * Created by Stefan on 10.11.2016.
 */

import javax.swing.*;

public class MainWindow
{
    public static void main(String[] args)
    {
        //tag::window[]
        JFrame frame = new JFrame("Cepheus");
        //javax.swing.border.TitledBorder dragBorder = new javax.swing.border.TitledBorder( "Drop 'em" );
        final JTextArea text = new JTextArea();
        frame.getContentPane().add(
                new JScrollPane(text),
                java.awt.BorderLayout.CENTER);

        new FileDrop(System.out, text, new FileDrop.Listener()
        {
            public void filesDropped(java.io.File[] files)
            {
                for (int i = 0; i < files.length; i++)
                {
                    try
                    {
                        text.append(files[i].getCanonicalPath() + "\n");
                    } catch (java.io.IOException e)
                    {
                    }
                }// end for: through each dropped file
            }// end filesDropped
        });
        //tag::window[]

        frame.setBounds(100, 100, 300, 400);
        frame.setDefaultCloseOperation(frame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
